import "./assets/style/vendor.css"
import "./assets/style/App.css"
import QuizBlock from "./components/QuizBlock"
import { useState } from "react"

function App() {
  const [start, setStart] = useState(false)

  const handleStart = () => {
    setStart(true)
  }

  return (
    <div className="container">
      <div className="content">
        <h2>Simple Quiz:</h2>
        {start ? (
          <QuizBlock start={start} />
        ) : (
          <button className="btn-start" role="button" onClick={handleStart}>
            Start
          </button>
        )}
      </div>
    </div>
  )
}

export default App
