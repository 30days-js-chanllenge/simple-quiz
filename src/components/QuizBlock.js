import { useEffect, useRef, useState } from "react"
import { answers, questions } from "../assets/js/QnA"
import { formatTime } from "../assets/js/time"
import { Compare } from "../assets/js/compare"
import { UpdateAns, checkArrContainObj } from "../assets/js/check"
import Timer from "./Timer"

const QuizBlock = ({ start }) => {
  const [cur, setCur] = useState(0)
  const [count, setCount] = useState(null)
  const [checked, setChecked] = useState([])
  const [submit, setSubmit] = useState(false)
  const [redo, setRedo] = useState(0)
  const [objCompare, setObjCompare] = useState([])

  const intervalIdRef = useRef(null)
  const oldCheck = useRef([])

  useEffect(() => {
    if (submit) {
      clearInterval(intervalIdRef.current)
    }
  }, [submit])

  const handleSubmit = (e) => {
    e.preventDefault()
    setSubmit(true)

    const [countSame, detailCompare] = Compare(answers, checked)
    setCount(countSame)
    setObjCompare(detailCompare)
  }

  const handleRedo = () => {
    setRedo((redo) => redo + 1)
    setCur(0)
    setCount(0)
    setSubmit(false)
    oldCheck.current = JSON.parse(JSON.stringify(checked))
  }

  return (
    <div className="quiz-block">
      <h4 className="order">
        Question {cur + 1} of {questions.length}:
      </h4>
      <Timer start={start} intervalIdRef={intervalIdRef} redo={redo} />
      <p>{questions[cur]?.ques || ""}</p>
      {questions[cur]?.ans.map((a) => {
        console.log("check", checked, oldCheck.current)
        return (
          <div className="quiz-ans-item" key={a.idA}>
            <input
              type="radio"
              id={a.idA}
              name={`answer_${questions[cur]?.id}`}
              value={a.idA}
              onChange={() => {
                const newItem = { [questions[cur]?.id]: a.idA }
                const updateAns = UpdateAns(checked, newItem)
                setChecked(updateAns)
              }}
              checked={checkArrContainObj(checked, {
                [questions[cur]?.id]: a.idA,
              })}
              className={
                checkArrContainObj(oldCheck.current, {
                  [questions[cur]?.id]: a.idA,
                })
                  ? objCompare[questions[cur]?.id] == true
                    ? "ans-true"
                    : "ans-false"
                  : ""
              }
            />
            <label htmlFor={a.idA}> {a.content}</label>
          </div>
        )
      })}

      {cur !== 0 && !submit ? (
        <button
          className="btn btn-next"
          role="button"
          onClick={() => setCur(cur - 1)}
        >
          Previous
        </button>
      ) : (
        <></>
      )}
      {cur + 1 < questions.length && (
        <button
          className="btn btn-next"
          role="button"
          onClick={() => setCur(cur + 1)}
        >
          Next
        </button>
      )}
      {cur + 1 >= questions.length && (
        <button className="btn btn-submit" role="button" onClick={handleSubmit}>
          Submit
        </button>
      )}
      {submit && (
        <button className="btn btn-next" role="button" onClick={handleRedo}>
          Redo
        </button>
      )}
      {submit && (
        <div className="done-block">
          <p>You answered correctly in {count} questions}</p>
          {/* phao hoa firework */}
          <div class="pyro">
            <div class="before"></div>
            <div class="after"></div>
          </div>
        </div>
      )}
    </div>
  )
}

export default QuizBlock
