import { useEffect, useState } from "react"
import { formatTime } from "../assets/js/time"

export default function Timer({ start, intervalIdRef, redo }) {
  const [timer, setTimer] = useState(0)
  useEffect(() => {
    if (start) {
      intervalIdRef.current = setInterval(() => {
        setTimer((prevTimer) => prevTimer + 1)
      }, 1000)
    }
    return () => clearInterval(intervalIdRef.current)
  }, [start, redo])
  return <div className="timer">Time : {formatTime(timer)}</div>
}
