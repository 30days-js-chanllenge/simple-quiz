const answers = [{ 1: 4 }, { 2: 6 }, { 3: 12 }]

const questions = [
  {
    id: 1,
    ques: "1 cộng 1 bằng mấy ?",
    ans: [
      { idA: 1, content: "A. Không biết" },
      { idA: 2, content: "B. Don't know" },
      { idA: 3, content: "C. Chắc 1" },
      { idA: 4, content: "D. Chắc 2" },
    ],
  },
  {
    id: 2,
    ques: "2 cộng 2 bằng mấy",
    ans: [
      { idA: 5, content: "A. 5" },
      { idA: 6, content: "B. 4" },
      { idA: 7, content: "C. 0" },
      { idA: 8, content: "D. 10" },
    ],
  },
  {
    id: 3,
    ques: "2 cộng 0 bằng mấy",
    ans: [
      { idA: 9, content: "A. 5" },
      { idA: 10, content: "B. 4" },
      { idA: 11, content: "C. 0" },
      { idA: 12, content: "D. 2" },
    ],
  },
]

export { answers, questions }
