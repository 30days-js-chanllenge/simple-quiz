function Compare(sysArr, userArr) {
  let countSame = 0
  let detailCompare = {}

  userArr.forEach((user) => {
    const foundSys = sysArr.find((sys) => {
      return (
        Object.keys(sys)[0] === Object.keys(user)[0] &&
        Object.values(sys)[0] === Object.values(user)[0]
      )
    })

    if (foundSys) {
      countSame += 1
      detailCompare = { ...detailCompare, [Object.keys(user)[0]]: true }
    } else {
      detailCompare = { ...detailCompare, [Object.keys(user)[0]]: false }
    }
  })
  return [countSame, detailCompare]
}

export { Compare }
