// kiểm tra xem ans nào đc chọn cho các qués đã chọn
const checkArrContainObj = (x, targetObject) => {
  let containsTarget = x.some(
    (item) => JSON.stringify(item) === JSON.stringify(targetObject)
  )

  if (containsTarget) {
    return true
  } else {
    return false
  }
}

//uopdate nêú thay đổi đáp án hoặc thêm đáp án vào nếu chưa chọn
const UpdateAns = (A, B) => {
  // Kiểm tra xem key của B đã có trong các key của A hay chưa
  let isKeyExist = A.some((item) =>
    Object.keys(item).some((key) => key === Object.keys(B)[0])
  )

  // Nếu key của B đã có trong A, cập nhật giá trị của key đó
  if (isKeyExist) {
    A = A.map((item) => {
      let key = Object.keys(item)[0]
      if (key === Object.keys(B)[0]) {
        item[key] = B[key]
      }
      return item
    })
  } else {
    // Nếu key của B chưa có trong A, thêm đối tượng B vào mảng A
    A.push(B)
  }
  return A
}

export { checkArrContainObj, UpdateAns }
